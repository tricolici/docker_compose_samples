#!/bin/bash
set -e

echo "echo $PHP_INI_DIR"

exec /usr/local/sbin/php-fpm --force-stderr --nodaemonize
